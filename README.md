# poe-filter-mode

| poe-filter-mode   | 1.4.0                              |
| ----------------- | ---------------------------------- |
| Maintainer        | Allele Dev (allele.dev@gmail.com)  |
| Copyright         | Copyright (C) 2022 Allele Dev      |
| License           | GPL-3                              |
| POE Version       | 3.19                               |

## Overview

An emacs minor-mode for highlighting [Path of
Exile](https://www.pathofexile.com/) item filter files.

This is the item filter files
[wiki](https://pathofexile.gamepedia.com/Item_filter_guide) spec.

Tested against [NeverSink](https://github.com/NeverSinkDev/NeverSink-Filter).

Using the
[grandshell](https://github.com/steckerhalter/grandshell-theme) theme,
it looks like:

![NeverSink highlighting](./images/never-sink-poe-filter-mode.jpg)

## Installation

### Manually

As long as you have a version of emacs that includes the `package`
library, the following should work:

* download it or `git clone` it locally
* load emacs

```
<M-x> package-install-file <enter>
Package file name: poe-filter-mode.el
```

Then, add the following to your init.el:

```emacs-lisp
(add-to-list 'auto-mode-alist '("\\.filter\\'" . poe-filter-mode))
```

This makes it so that all files ending in `.filter` are handled by
`poe-filter-mode`.

It should Do The Right Thing at this point, whenever you open an item
filter file.

## Licensing

This project is distributed under the GPL-3 license. See the included
[LICENSE](./LICENSE) file for more details.
