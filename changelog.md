# 1.3.0 (Dec. 25, 2019)

* Add support for Path of Exile 3.9.0 filter changes

# 1.2.0 (Dec. 21, 2018)

* Add support for elder, shaper, map icons, and effects

# 0.1.1 (Dec. 7, 2017)

* init 
