;;; poe-filter-mode.el --- A path of exile filter file mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Allele Dev

;;; Author: Allele Dev <allele.dev@gmail.com>
;;; Version: 1.4.0
;;; Maintainer: Allele Dev <allele.dev@gmail.com>
;;; Created: 20 Aug 2022
;;; Keywords: games
;;; Homepage: https://gitlab.com/queertypes/poe-filter-mode
;;; Package-Requires: ((emacs "25.3"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides syntax highlighting for Path of Exile item
;; filter files. Updated for 3.19.

;;; Code:

(defvar poe-filter-font-lock nil "Font faces for filter files.")
(defvar poe-filter-mode-syntax-table nil "Syntax for filter files.")

;; Highlights keywords, functions, constants, and built-ins.
(setq poe-filter-font-lock
      (let* (

             ;; Filter language matchable properties.
             (x-keywords '("AreaLevel"
                           "ItemLevel"
                           "DropLevel"
                           "Quality"
                           "Rarity"
                           "Class"
                           "BaseType"
                           "Prophecy"
                           "LinkedSockets"
                           "SocketGroup"
                           "Sockets"
                           "Height"
                           "Width"
                           "HasExplicitMod"
                           "AnyEnchantment"
                           "HasEnchantment"
                           "EnchantmentPassiveNode"
                           "EnchantmentPassiveNum"
                           "StackSize"
                           "GemLevel"
                           "GemQualityType"
                           "AlternateQuality"
                           "Replica"
                           "Identified"
                           "Corrupted"
                           "CorruptedMods"
                           "Mirrored"
                           "ElderItem"
                           "ShaperItem"
                           "HasInfluence"
                           "HasSearingExarchImplicit"
                           "HasEaterOfWorldsImplicit"
                           "FracturedItem"
                           "SynthesizedItem"
                           "ElderMap"
                           "ShaperMap"
                           "BlightedMap"
                           "MapTier"))

            ;; Filter language callable functions.
            (x-functions '("SetBorderColor"
                           "SetTextColor"
                           "SetBackgroundColor"
                           "SetFontSize"
                           "PlayAlertSound"
                           "PlayAlertSoundPositional"
                           "DisableDropSound"
                           "EnableDropSound"
                           "CustomAlertSound"
                           "MinimapIcon"
                           "PlayEffect"))

            ;; Constant properties that can be matched against.
            ;; Not exhaustive.
            (x-constants '("Magic"
                           "Rare"
                           "Unique"
                           "Normal"
                           "True"
                           "False"
                           "RGB"
                           "WWW"
                           "White"
                           "Red"
                           "Blue"
                           "Green"
                           "Yellow"
                           "Brown"
                           "Cyan"
                           "Grey"
                           "Orange"
                           "Pink"
                           "Purple"
                           "Circle"
                           "Diamond"
                           "Hexagon"
                           "Square"
                           "Star"
                           "Triangle"
                           "Cross"
                           "Moon"
                           "Raindrop"
                           "Kite"
                           "Pentagon"
                           "UpsideDownHouse"
                           "Temp"
                           "Shaper"
                           "Elder"
                           "Warlord"
                           "Redeemer"
                           "Hunter"
                           "Crusader"
                           "Superior"
                           "Divergent"
                           "Anomalous"
                           "Phantasmal"))

            ;; The main part of the filter - show or hide.
            (x-builtins '("Show" "Hide" "Continue"))

            ;; generate regex string for each category of keywords
            (x-constants-regexp (regexp-opt x-constants 'words))
            (x-builtins-regexp (regexp-opt x-builtins 'words))
            (x-functions-regexp (regexp-opt x-functions 'words))
            (x-keywords-regexp (regexp-opt x-keywords 'words)))

        `(
          (,x-constants-regexp . font-lock-constant-face)
          (,x-builtins-regexp . font-lock-builtin-face)
          (,x-functions-regexp . font-lock-function-name-face)
          (,x-keywords-regexp . font-lock-keyword-face))))

;; Handle comment syntax properly.
(setq poe-filter-mode-syntax-table
      (let ((synTable (make-syntax-table)))
        (modify-syntax-entry ?# "<" synTable)
        (modify-syntax-entry ?\n ">" synTable)
        synTable))

;;;###autoload
(define-derived-mode poe-filter-mode prog-mode "poe-filter mode"
  "Major mode for editing Path of Exile filter files."

  ;; code for syntax highlighting
  (set-syntax-table poe-filter-mode-syntax-table)
  (setq font-lock-defaults '((poe-filter-font-lock))))

;; add the mode to the `features' list
(provide 'poe-filter-mode)

;;; poe-filter-mode.el ends here
